

Spring is a good choice for framework, as it have good documentation, examples, and would be easy to hire people who could develop with it. 
I selected Boot, as I prefer microservices instead of monolitic servers. 

There are 3 layers, and their responsibilities described as comments. 

Lombok is to speedup prototyping, but I prefer to use also in late project stages.
I prefer gradle to maven or and, as it is easier to read configuration, and possible to mix local libs and libs from repositories (not needed in this project however)



I hope exception handling were not part of task. It could be done something like that: 

@ResponseStatus(value= HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException {

  public BadRequestException() {
    this("Invalid data.");
  }

  public BadRequestException(String msg) {
    super(msg);
  }
}

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
 @ExceptionHandler(value = {BadRequestException.class})
  @ResponseBody
  protected ResponseEntity<Object> badRequest(RuntimeException ex, WebRequest request) {
    log.trace("Bad request.");
    log.trace("    Request:  {}", request);
    log.trace("    Exception: ", ex);

    return handleExceptionInternal(ex, ex.getMessage(),
        new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
  }
}


To start server, use:
gradle bootRun