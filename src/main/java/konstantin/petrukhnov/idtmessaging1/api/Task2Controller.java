package konstantin.petrukhnov.idtmessaging1.api;

import konstantin.petrukhnov.idtmessaging1.service.Task2Service;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

/**
 * Created by konstantin.petrukhnov@gmail.com on 2017-01-03.
 */
@Slf4j
@RestController
@RequestMapping("/task2")
public class Task2Controller {

    @Autowired
    private Task2Service task2Service;

    /**
     * Assuming name will could be part of url (e.g. no special characters or spaces)
     * @param name
     * @param index
     * @return
     */
    @RequestMapping(value = "/{name}/{index}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public HashMap get(@PathVariable("name") String name, @PathVariable("index") Integer index) {
        return task2Service.get(name, index);
    }


}
