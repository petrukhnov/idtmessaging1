package konstantin.petrukhnov.idtmessaging1.api;

import konstantin.petrukhnov.idtmessaging1.model.User;
import konstantin.petrukhnov.idtmessaging1.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

/**
 * Controller layer is responsible for exchanging data between server and clients. Handling presentation of data.
 * It also responsible for validation of data formats, and presenting error.
 * May access multiple services to get/combine/validate data, but not other controllers or repositories.
 *
 * Annotations are default way for spring rest.
 *
 * @Slf4j annotation just add convenient logger (slf4j+logback) to class.
 *
 * Created by konstantin.petrukhnov@gmail.com on 2017-01-03.
 */
@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public User get(@PathVariable("id") String id) {
        return userService.get(id);
    }

    @RequestMapping(value="/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public User update(@RequestBody User user, @PathVariable("id") String id) {
        /*
        Here should be some validation against injecting id into body. E.g. with annotation, or just directly in the method body:
        if (user.getId()!=null) return exception

        This is good enough to avoid attack towards described vector and encapsulate property to single object that will be passed.
         */
        user.setId(id);

        return userService.update(user);
    }

}
