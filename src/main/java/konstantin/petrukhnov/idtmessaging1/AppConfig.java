package konstantin.petrukhnov.idtmessaging1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by konstantin.petrukhnov@gmail.com on 2017-01-03.
 */
@SpringBootApplication
public class AppConfig {

    public static void main(String[] args) {
        SpringApplication.run(AppConfig.class, args);

    }
}
