package konstantin.petrukhnov.idtmessaging1.repository;

import konstantin.petrukhnov.idtmessaging1.model.User;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

/**
 * Repository layer is responsible for acquiring or persisting data. It should not access other repositories or services.
 *
 * Created by konstantin.petrukhnov@gmail.com on 2017-01-03.
 */
@Repository
public class UserRepository {

    Map<String, User> mockedUsers = new HashMap();

    @PostConstruct
    public void createMockedData() {
        User mockedUser1 = new User();
        mockedUser1.setId("1");
        mockedUser1.setName("Pekka");
        mockedUser1.setDetails("insane");
        mockedUsers.put("1", mockedUser1);

        User mockedUser2 = new User();
        mockedUser2.setId("2");
        mockedUser2.setName("Jussi");
        mockedUser2.setDetails("");
        mockedUsers.put("2", mockedUser2);

    }

    public User get(String id) {
        return mockedUsers.get(id);
    }

    public User update(User newUser) {

        User savedUser = get(newUser.getId());
        if (savedUser==null){
            //todo throw exception
        }

        //update only non-null fields, and persist
        // just example, could be done in generic way for most fields (except non-tree graphs), directly saving to DB
        //e.g. with MongoDB it could create separate update object with only fields to be updated.

        if(newUser.getName()!=null) {
            savedUser.setName(newUser.getName());
        }
        if(newUser.getDetails()!=null) {
            savedUser.setDetails(newUser.getDetails());
        }

        return savedUser;
    }
}
