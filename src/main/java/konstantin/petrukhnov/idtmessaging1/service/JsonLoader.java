package konstantin.petrukhnov.idtmessaging1.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by konstantin.petrukhnov@gmail.com on 2017-01-03.
 */
@Slf4j
@Service
public class JsonLoader {

    //@Value("${task2.folder}")
    String jsonConfigFolder = "task2/";

    @Autowired
    private Task2Service task2Service;

    @PostConstruct
    public void init() throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        File folder = ResourceUtils.getFile("classpath:"+jsonConfigFolder);

        log.debug("files: {}", folder.listFiles().length);

        //assuming that no files are deleted during read
        for (File jsonFile : folder.listFiles()) {
            //data is parsed directly into map, as only requirement were to show it. Alternatively it could be converted to pojo, by writing custom converter
            HashMap<String, ArrayList> obj = mapper.readValue(jsonFile, HashMap.class);
            //assuming that all files are well formed
            Map.Entry<String, ArrayList> entry = obj.entrySet().iterator().next();
            task2Service.addData(entry.getKey(), entry.getValue());

        }
    }

}
