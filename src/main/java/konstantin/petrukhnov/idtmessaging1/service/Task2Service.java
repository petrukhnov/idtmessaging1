package konstantin.petrukhnov.idtmessaging1.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by konstantin.petrukhnov@gmail.com on 2017-01-03.
 */
@Slf4j
@Service
public class Task2Service {

    //container for parsed data
    private HashMap<String, ArrayList<HashMap>> jsondata = new HashMap<>();


    public void addData(String name, ArrayList<HashMap> list) {
        jsondata.put(name,list);
    }

    public HashMap get(String name, Integer index) {
        //npe checks or exceptions could be added here or other places, based on requirements
        return jsondata.get(name).get(index);
    }


}
