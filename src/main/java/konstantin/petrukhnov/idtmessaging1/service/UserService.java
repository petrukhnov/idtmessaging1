package konstantin.petrukhnov.idtmessaging1.service;

import konstantin.petrukhnov.idtmessaging1.model.User;
import konstantin.petrukhnov.idtmessaging1.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service is responsible for domain(business) logic. It know which parts need to be connected to make desirable result.
 * It could access multiple repositories or other services.
 *
 * Created by konstantin.petrukhnov@gmail.com on 2017-01-03.
 */
@Slf4j
@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;


    public User get(String id) {
        return userRepository.get(id);
    }

    public User update(User user) {
        log.debug("get {}", user.getId());
        return userRepository.update(user);
    }
}
