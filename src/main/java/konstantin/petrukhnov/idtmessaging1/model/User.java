package konstantin.petrukhnov.idtmessaging1.model;

import lombok.Data;

/**
 * Model classes define business model and provide communication protocol between server components.
 * E.g. instead of calling repository.update(id, description) in service, it is enough to call repository.update(user)
 * That will provide flexibility, so changes in method names, will not require changes in components that pass them to next components.
 *
 * Lombok data annotation will create empty constructor and all getters/setters.
 * Making objects easier to read and modify.
 *
 * Created by konstantin.petrukhnov@gmail.com on 2017-01-03.
 */
@Data
public class User {

    private String id;
    private String name;
    private String details;
}
