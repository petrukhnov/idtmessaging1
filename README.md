# Solutions for IDT Messaging tasks


## Task 1 (Java/JVM) 

Write a simple server, which provides a RESTful API.
Technologies to use: JVM

The server should implement these two API calls:

GET /user/{id}
Returns user details based on the id. You can return static mock results.
PUT /user/{id}
Modifies a user's property. It's ok to only store changes only in memory.

## Task 2 (Java / JVM)

Make a Java app which parses the two JSON strings below into JSON objects.

The app should be able to retrieve an arbitrary attribute of N:th element in the response.

JSON 1:

{
"businesses": [
{
"id": "yelp-tropisueno",
"name" : "Tropisueno",
"display_phone": "+1-415-908-3801",
"image_url": "http://s3-media2.ak.yelpcdn.com/bphoto/7DIHu8a0AHhw-BffrDIxPA/ms.jpg",
"description": ""
}
]
}

JSON 2:

{
"shops": [
{
"id": "sample-shop",
"name" : "Mingalaba",
"display_phone": "+1-408-366-6600",
"image_url": "http://s3-media2.ak.yelpcdn.com/bphoto/7DIHu8a0AHhw-BffrDIxPA/ms.jpg",
"description": "Best Asian lovingly prepare by Jack "Papa" Rasmussen"
}
]
}

## Task 3 (Architecture)

Describe on a high level how you would design a messaging backend.

- Provide an architecture schematic
- Discuss scalability and performance issues